# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice External Reference',
    'name_de_DE': 'Fakturierung Externer Beleg',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds the field for External Reference on Invoices
''',
    'description_de_DE': '''
    - Fügt Feld Externer Beleg auf Rechnungen hinzu
''',
    'depends': [
        'account_invoice',
        'account_move_external_reference'
    ],
    'xml': [
        'invoice.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
