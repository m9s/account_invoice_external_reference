# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, In, And, Or

class Invoice(ModelWorkflow, ModelSQL, ModelView):
    _name = 'account.invoice'

    external_reference = fields.Char('External Reference',
            states={
                'readonly': Or(Not(Equal(Eval('state'), 'draft')),
                    In(Eval('type'), ['out_invoice', 'out_credit_note'])),
                # Do not set field required for draft invoices
                # as they can be created by another module without
                # knowledge of this value.
                'required': And(Not(Equal(Eval('state'), 'draft')),
                    In(Eval('type'), ['in_invoice', 'in_credit_note'])),
                }, depends=['state', 'type'])

    def __init__(self):
        super(Invoice, self).__init__()
        self.reference = copy.deepcopy(self.reference)
        if self.reference.states is None:
            self.reference.states = {
                'readonly': True
                }
        else:
            self.reference.states['readonly'] = True
        self._reset_columns()

    def _get_move_line(self, invoice, date, amount):
        '''
        Return move line
        '''
        res = super(Invoice, self)._get_move_line(invoice, date, amount)

        res['reference'] = invoice.number
        if invoice.type in ('out_invoice', 'out_credit_note'):
            res['external_reference'] = invoice.number
        else:
            res['external_reference'] = invoice.external_reference
        return res

Invoice()
